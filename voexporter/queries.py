import os
from glob import glob
import logging
from datetime import datetime
import cx_Oracle
import math

LTADSN = cx_Oracle.makedsn("db.lofar.target.rug.nl", 1521, service_name="db.lofar.target.rug.nl")
BATCHSIZE = 200
logger = logging.getLogger(__file__)
QUERIES_FILE_PATH = os.path.join(os.path.dirname(__file__), 'sqlqueries')


def find_query_files():
    return [file_path for file_path in glob(os.path.join(QUERIES_FILE_PATH, '*.sql'), recursive=False)]


def sanitize_value(value):
    if isinstance(value, datetime):
        return value.isoformat()
    elif type(value) == float and math.isnan(value):
        return 'nan'
    else:
        return value


def _process_cursor_reply(cursor, batch_size=BATCHSIZE):
    col_names, *_ = zip(*cursor.description)
    while True:
        rows = cursor.fetchmany(batch_size)
        if not rows:
            break
        yield [{col.lower(): sanitize_value(value) for col, value in zip(col_names, row)} for row in
               rows]


def query_data_products(connection: cx_Oracle.Connection, from_date):
    query_files = find_query_files()
    for query_file in query_files:
        query_name = os.path.basename(query_file.replace('.sql', '')).replace('_', ' ')
        logging.info('Processing query "%s" from file %s', query_name, query_file)
        with open(query_file, 'r') as f_stream:
            query = f_stream.read()
            cursor = connection.cursor()

            cursor.execute(query, [from_date])
            for bulk in _process_cursor_reply(cursor):
                yield bulk
