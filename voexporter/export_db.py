import couchdb
import urllib.parse as url_parse
from datetime import datetime
from typing import Union, List, Dict

ISOFORMAT="%Y-%m-%dT%H:%M:%S"
VO_EXPORTER_SERVICE_DB = 'vo_exporter_service'
VO_DATAPRODUCT = 'vo_dataproduct'


def export_db_create_connection(host, user, password):
    parsed_url = url_parse.urlparse(host)
    server = couchdb.Server(f'{parsed_url.scheme}://{user}:{password}@{parsed_url.netloc}')
    return server


def _get_or_create_db(server, db_name):
    if db_name not in server:
        server.create(db_name)

    return server[db_name]


def get_or_create_service_db(server: couchdb.Server):
    return _get_or_create_db(server, VO_EXPORTER_SERVICE_DB)


def get_or_create_dataproduct_db(server: couchdb.Server):
    return _get_or_create_db(server, VO_DATAPRODUCT)


def get_last_updated_datetime(server: couchdb.Server):
    service_obj = get_or_create_service_db(server)
    if 'service_info' not in service_obj:
        service_obj.save({'_id': 'service_info'})

    if 'last_updated' not in service_obj['service_info']:
        set_last_updated_datetime(server, datetime.strptime('1900-01-01T00:00:00', ISOFORMAT))
        return get_last_updated_datetime(server)
    else:
        return datetime.strptime(service_obj['service_info']['last_updated'], ISOFORMAT)


def set_last_updated_datetime(server: couchdb.Server, last_update_time: datetime):
    service_obj = get_or_create_service_db(server)
    if 'service_info' not in service_obj:
        service_obj.save({'_id': 'service_info'})

    service_info_obj = service_obj['service_info']
    service_info_obj['last_updated'] = last_update_time.strftime(ISOFORMAT)
    service_obj['service_info'] = service_info_obj


def put_observation_objects(server: couchdb.Server, item_or_items: Union[Dict, List[Dict]]):
    data_product_db = get_or_create_dataproduct_db(server)
    if isinstance(item_or_items, dict):
        item_or_items['_id'] = item_or_items['obs_publisher_did']
        data_product_db.save(item_or_items)
        set_last_updated_datetime(server, datetime.strptime(item_or_items['t_min'], ISOFORMAT))
    else:
        documents = [couchdb.Document(_id=item['obs_publisher_did'], **item) for item in item_or_items]
        data_product_db.update(documents)
        set_last_updated_datetime(server, datetime.strptime(item_or_items[-1]['t_min'], ISOFORMAT))

