#!/usr/bin/env python
from argparse import ArgumentParser
import logging
import configparser
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from datetime import timezone
import cx_Oracle
import os
from apscheduler.schedulers.background import BlockingScheduler

from voexporter.queries import query_data_products, LTADSN
from voexporter.export_db import export_db_create_connection, get_last_updated_datetime, put_observation_objects

scheduler = BlockingScheduler(daemon=True)
configuration = None
logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.INFO)


def configure(config_path):
    global configuration
    config_parser = configparser.ConfigParser()
    default_config_path = [
        os.path.join(os.path.expanduser('~'),
                     '.config',
                     'lta_vo_exporter',
                     'config.ini')]

    if config_path:
        default_config_path = [config_path] + default_config_path

    found_configs = config_parser.read(default_config_path)
    if found_configs:
        for config_path in found_configs:
            logging.info('using configuration file %s', os.path.abspath(config_path))
        configuration = config_parser
    else:
        logging.error('Cannot find configuration file please specify it manually')
        raise SystemExit(1)


def parse_args():
    parser = ArgumentParser(description='LTA VO exporter service')
    parser.add_argument('--config', help='Path to configuration file')
    return parser.parse_args()


def configure_scheduler():
    jobstores = {
        'default': SQLAlchemyJobStore(url='sqlite://jobs.sqlite')
    }
    executors = {
        'default': {'type': 'threadpool', 'max_workers': 10},
    }
    job_defaults = {
        'coalesce': True,
        'max_instances': 1
    }
    scheduler.configure(jobstores=jobstores, executors=executors, job_defaults=job_defaults, timezone=timezone.utc)


def update_observations_db():
    lta_connection = cx_Oracle.connect(dsn=LTADSN,
                                       user=configuration['LTA']['user'],
                                       password=configuration['LTA']['password'])

    edb_connection = export_db_create_connection(host=configuration['EXPORTDB']['host'],
                                                 user=configuration['EXPORTDB']['user'],
                                                 password=configuration['EXPORTDB']['password'])

    from_date = get_last_updated_datetime(edb_connection)
    for batch_id, result in enumerate(query_data_products(lta_connection, from_date)):
        logging.info('inserting data products from batch %s', batch_id)
        put_observation_objects(edb_connection, result)


def configure_tasks():
    scheduler.add_job(update_observations_db, trigger='interval',
                      minutes=int(configuration['DEFAULT']['schedule_interval_in_minutes']))


def main():
    args = parse_args()
    configure(args.config)
    configure_tasks()
    try:
        logging.info('Starting scheduler')
        while True:
            scheduler.start()
    except KeyboardInterrupt:
        logging.info('Interruption by user')
    except Exception as e:
        logging.exception(e)

    logging.info('Stopping scheduler')
    scheduler.shutdown()


if __name__ == '__main__':
    main()
