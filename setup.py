from setuptools import setup, find_packages
from glob import glob
import os.path

def find_sql_queries(path):
    return [fname for fname in glob(os.path.join(path, '*.sql'))]


setup(
    name='lta_voexporter',
    version='0.0.1',
    description='LTA VO exporter',
    author='Mattia Mancini',
    author_email='mancini@astron.nl',
    url='https://blog.godatadriven.com/setup-py',
    packages=find_packages(include=['voexporter', 'voexporter.*']),
    install_requires=[
        'cx_Oracle',
        'couchdb',
        'apscheduler',
        'sqlalchemy'
    ],
    data_files=[('voexporter/sqlqueries', find_sql_queries('voexporter/sqlqueries'))],
    include_package_data=True,
    scripts=['bin/lta_voexporter.py'],
    setup_requires=['pytest-runner', 'flake8'],
    tests_require=['pytest']
)
